import rpg
hall = rpg.Room("Hall")
hall.set_description("Flower pots blossoming along the walls.")
ghost = rpg.Enemy("Abigail", "An angry ghost")
ghost.set_conversation("If you don't leave me alone I will posses your body.")


# Items
mirror = rpg.Item("Mirror")
mirror.set_description("Big mirror over a small table with lion like legs.")


hall.set_item(mirror)
feather = rpg.Item("Feather")
feather.set_description("Peacock feather. Long and beautiful")
ghost.set_weakness("Feather")
ghost.set_neutral_items("Mirror")
hall.set_character(ghost)

# Game logic

current_room = hall
directions = ["North", "South", "East", "West"]
backpack =["Axe", "Feather"]
keep_playing = True
things = []

# text
welcome = "This is a text adventure, type in what you want to do. Type help to see the actions that you can try."
movements = "You can move " + str(directions)
orders = "or try: talk, fight, hug, take, inventory"

print(welcome)

while keep_playing:
    print("\n")
    current_room.get_details()
    
    inhabitant = current_room.get_character()
    if inhabitant is not None:
        inhabitant.describe()

    thing = current_room.get_item()
    if thing is not None:
        thing.describe()
        
    command = input("> ")

    if command.capitalize() in directions:
        # Move in the given direction
        current_room = current_room.move(command.capitalize())
    
    elif command.lower() == "talk":
        # Talk to the inhabitant, if there is one
        if inhabitant is not None:
            inhabitant.talk()
        else:
            print("Are you talking alone?")

    elif command.lower() == "fight":
        # Fight with the inhabitant if there is one    
        if inhabitant is None:
            print("There is no one here to fight with")
        else:  
            print("What will you fight with?")
            fight_with = input()

            # Do I have this item ?
            print(fight_with) #debug
            print(backpack) #debug
            print(ghost.weakness) #debug
            if fight_with in backpack:
                
                if inhabitant.fight(fight_with) == 0:
                    current_room.set_character = None
                    inhabitant.foes -= 1
                    print("Hooray, you won the fight!")
                    if inhabitant.foes == 0:
                        keep_playing = False
                elif inhabitant.fight(fight_with) == 1:
                    print("Think, what you have learn about this enemy?")
                else:
                    keep_paying = False
            else:
                print("you don't have " + fight_with + " in your backpak")
       
    elif command.lower() == "take":
        if thing is not None:
            print("You put the " + thing.get_name() + " in your backpack")
            backpack.append(thing.get_name())
            current_room.set_item(None)
        else:
            print("There is nothing here to take")

    elif command.lower() == "hug":
        # Hug the inhabitant, if there is one
        if inhabitant is not None:
            inhabitant.hug()
        else:
            print("There is nobody to hug here")
          
    elif command.lower() == "help":
        print(movements)
        print(orders)

    elif command.lower() == "inventory":
        if backpack == []:
            print("There is nothing in your back pack")
        else:
            print("In your backpack there are the following items:")
            print(backpack)
            print("Enemies = " + str(ghost.foes))

    elif command.lower() == "quit":
        keep_playing = False

    else:
        print("I don't know how to " + command)

# Game over
go = "GAME OVER"
line = "-" * len(go)
print(go)
print(line)

if ghost.foes != 0:
  print("It was a dumb way to waste your life. Valient, but dumb nevertheless.")
else:
  print("You succeded in this quest!")
