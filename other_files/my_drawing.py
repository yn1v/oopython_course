# https://gitlab.com/yn1v/oopython_course
# Neville A. Cross
#

from shapes import Triangle, Rectangle, Oval

rect1 = Rectangle()
rect1.set_width(600)
rect1.set_height(300)
rect1.set_color("blue")
rect1.set_y(0)
rect1.set_x(0)
rect1.draw()

rect2 = Rectangle()
rect2.set_width(600)
rect2.set_height(100)
rect2.set_color("white")
rect2.set_y(100)
rect2.set_x(0)

rect2.draw()

circle1 = Oval()
circle1.set_height(100)
circle1.set_width(100)
circle1.set_y(100)
circle1.set_x(250)
circle1.draw()

circle2 = Oval()
circle2.set_height(96)
circle2.set_width(96)
circle2.set_y(102)
circle2.set_x(252)
circle2.set_color("white")
circle2.draw()


tri = Triangle(255,175,345,175,300,105)
tri.draw()

tri = Triangle(260,170,340,170,300,110)
tri.set_color("white")
tri.draw()



