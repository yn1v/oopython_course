# https://gitlab.com/yn1v/oopython_course
# Neville A. Cross
#

from shapes import Triangle, Rectangle, Oval

xa = 255
ya = 175
xb = 345
yb = 175
xc = 300
yc = 105


for i in range(10):
    tri = Triangle(xa,ya,xb,yb,xc,yc)
    if i%2 == 0:
        tri.set_color("white")
    else:
        tri.set_color("red")
    tri.draw()
    xa += 5
    ya -= 5
    xb -= 5
    yb -= 5
    # xc += 5
    yc -= 5

    




