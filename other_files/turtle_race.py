# https://gitlab.com/yn1v/oopython_course
# Neville A. Cross
#

from turtle import Turtle
from random import randint

don = Turtle()
don.color('purple')
don.shape('turtle')
don.penup()
don.goto(-160,100)
don.pendown()

mike = Turtle()
mike.color('orange')
mike.shape('turtle')
mike.penup()
mike.goto(-160,70)
mike.pendown()

raph = Turtle()
raph.color('red')
raph.shape('turtle')
raph.penup()
raph.goto(-160,40)
raph.pendown()

leo = Turtle()
leo.color('blue')
leo.shape('turtle')
leo.penup()
leo.goto(-160,10)
leo.pendown()

for movement in range(100):
  don.forward(randint(1,5))
  mike.forward(randint(1,5))
  raph.forward(randint(1,5))
  leo.forward(randint(1,5))
