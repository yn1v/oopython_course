# https://gitlab.com/yn1v/oopython_course
# Neville A. Cross
#

from room import Room
kitchen = Room("Kitchen")
kitchen.set_description("Where the Gods of Home live.\nGod of Fire, Goddess of Water, God of Bread and Goddess of Wine")
kitchen.describe()

dining = Room("Dinning hall")
dining.set_description("Where the Master meet the pleasent Gods.\nGod of Bread and Goddess of Wine")
dining.describe()

ballroom = Room("Ballroom")
ballroom.set_description("Where the intrigue is made.\God of Fire plays with Goddess of Wine")
ballroom.describe()
