import rpg
ghost = rpg.Enemy("Abigail", "An angry ghost")
ghost.set_conversation("If you don't leave me alone I will posses your body.")

gargoyle = rpg.Enemy("Gary", "An ugly Gargoyle carved out of stone.")
gargoyle.set_conversation("I am the most beautiful being in this house.")

mirror = rpg.Item("Mirror")
mirror.set_description("Big mirror over a small table with lion like legs.")

feather = rpg.Item("Feather")
feather.set_description("Peacock feather. Long and beautiful")

gargoyle.set_weakness("Mirror")
ghost.set_weakness(feather)

print("This is an object")
print(ghost.weakness)
print("This is a string")
print(gargoyle.weakness)
