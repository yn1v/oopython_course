"""
https://gitlab.com/yn1v/oopython_course
Neville A. Cross
"""

class Room():
    def __init__(self, room_name):
        """Creates one room"""
        self.name = room_name
        self.description = None
        self.linked_rooms = {}
        self.item = None
        self.character = None

    def set_character(self, new_character):
        """Define which character will be located in the room"""
        self.character = new_character
    
    def get_character(self):
        """Return the character located in the room"""
        return self.character

    def set_description(self, room_description):
        """Set the description for de room"""
        self.description = room_description

    def get_description(self):
        """Returns the room's description"""
        return self.description
      
    def get_name(self):
        """Return de room's name"""
        return self.name

    def set_name(self, room_name):
        """Set the room's name"""
        self.name = room_name

    def set_item(self, item_name):
        """Set an item located in the room"""
        self.item = item_name

    def get_item(self):
        """Returns the item located in the room"""
        return self.item

    def describe(self):
        """Prints the room's description"""
        print(self.description)

    def link_room(self, room_to_link, direction):
        """Links the room to the provided room in the provided direction"""
        self.linked_rooms[direction] = room_to_link
        
    def get_details(self):
        """Get all room's details"""
        print(self.name)
        line = "-" * len(self.name)
        print(line)
        print(self.description)
        for direction in self.linked_rooms:
            room = self.linked_rooms[direction]
            print("The", room.get_name(), "is", direction)
    
    def move(self, direction):
        """Routine for moving in one direction"""
        if direction in self.linked_rooms:
            return self.linked_rooms[direction]
        else:
            print("You can' t go that way")
            return self

  
