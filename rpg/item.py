"""
https://gitlab.com/yn1v/oopython_course
Neville A. Cross
"""

class Item():
    def __init__(self, item_name):
        """Creates an Item"""
        self.name = item_name
        self.description = None

    def set_name(self, item_name):
        """ Define the item's name """
        self.name = item_name

    def get_name(self):
        """ Returns the item's name """
        return self.name

    def set_description(self, item_description):
        """ Define the item's description """
        self.description = item_description

    def get_description(self):
        """ Returns the item's description """
        return self.description

    def describe(self):
        """ Print the item's description """
        print("The [" + self.name + "] is here - " + self.description)
