"""
https://gitlab.com/yn1v/oopython_course 
Neville A. Cross
"""

class Character():
    # Create a character
    def __init__(self, char_name, char_description):
        """Instiantate a character"""
        self.name = char_name
        self.description = char_description
        self.conversation = None

  # Describe this character
    def describe(self):
        """Print the charcter description"""
        print( self.name + " is here!" )
        print( self.description )

  # Set what this character will say when talked to        
    def set_conversation(self, conversation):
        """Set what the character will say"""
        self.conversation = conversation

  # Talk to this character
    def talk(self):
        """Makes the charcter say its line"""
        if self.conversation is not None:
            print("[" + self.name + " says]: " + self.conversation)
        else:
            print(self.name + " doesn't want to talk to you")

  # Fight with this character
    def fight(self, combat_item):
        """Default fight rutine"""
        print(self.name + " doesn't want to fight with you")
        return True

    def hug(self):
        """Default hug rutine"""        
        print("[" + self.name + " says]: You don't want to do that")

        
class Enemy(Character):

    foes = 0
    
    def __init__(self, char_name, char_description):
        """Creates a character from the sub class Enemy"""
        super().__init__(char_name, char_description)
        self.weakness = None
        Enemy.foes += 1 # When a enemy is created increases

    def set_weakness(self, item_weakness):
        """Define the enemy's weakness"""
        self.weakness = item_weakness
        
    def get_weakness(self):
        """Return the enemy's weakness"""
        return self.weakness
  
    # Fight with an enemy
    def fight(self, combat_item):
        """Fight rutine for an enemy"""
        if combat_item == self.weakness:
            print("You fend " + self.name + " off with the " + combat_item)
            Enemy.foes -= 1
            return True
        else:
            print(self.name + " crushes you, puny adventurer!")
            return False

    
class Friend(Character):
    def __inti__(self, char_name, char_description):
        """Creates a character from the sub class Friend"""
        super().__init__(char_name, char_description)
        self.hint = "Good luck"
        
    def set_hint(self, hint):
        """Defines a hint to give"""        
        self.hint = hint
    
    def get_hint(self):
        """Returns the friend's hint"""
        return self.hint
  
    def hug(self):
        """Hug rutine for a friend"""
        print(self.name + " hugs you back!")
        print("[" + self.name + " says]: " + self.get_hint())
