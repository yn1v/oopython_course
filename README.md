# Object Oriented Principles in Python by Raspberry Pi Fundation.

The material in this software repository is based in the online training given by Raspberry Pi Fundation at Future Learn. You can see details of [Object Oriented Principles](https://www.futurelearn.com/courses/object-oriented-principles) Course to find more about. The course is under Creative Commons Licence. I am not writing material to use such license. So, as I am writing code that can be useful as example, I setting this as GPLv3.

The course project is a text adventure in a rpg fashion. You can try it out by running **main.py**

If you have any sugestion or corrections, please open a issue in [GitLab](https://gitlab.com/yn1v/oopython_course/issues).

Have a look at the map if you have trouble making a mental picture of it by just the description provided in the game.

![Text Adventure Map](/images/map.png)

The **other_files** folder contains just some coding excersices not needed for the rpg course project.
