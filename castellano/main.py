# https://gitlab.com/yn1v/oopython_course
# Neville A. Cross
# GPLv3
# March, 2019
#

import rpg

# Cuartos 
cocina = rpg.Cuarto("Cocina")
cocina.fijar_descripcion("La única luz es el fuego que produce humo. El suelo está húmedo y resvaloso.")

salon = rpg.Cuarto("Salón")
salon.fijar_descripcion("Candelabros de cristal cuelgan del alto cielo raso. El suelo es de cuadro blancos y negros")

invernadero = rpg.Cuarto("Invernadero")
invernadero.fijar_descripcion("Paredes y techos de vidrio. Una fuente y dos bancas de roca. Dos mesas de trabajo llenas de plantas contiguo a la pared. Aire húmedo con aroma a menta")

comedor = rpg.Cuarto("Comedor")
comedor.fijar_descripcion("Una gran mesa para doce personas. Iluminado por las velas en la mesa. La gente seria en los cuadros parece hacer una triste danza.")

sotano = rpg.Cuarto("Sótano")
sotano.fijar_descripcion("Luz parpadeante y un piso con huecos. Muchas repisas.")

billar = rpg.Cuarto("Billar")
billar.fijar_descripcion("Hay una lámpara baja sobre la mesa de billar en el centro de la habitación. Huellas de tiza en todos lados.")

biblioteca = rpg.Cuarto("Biblioteca")
biblioteca.fijar_descripcion("Sillas confortables y un diván de velvet. Buena iluminación y miles de libros en diversos idiomas.")

estar = rpg.Cuarto("Salón de Estar")
estar.fijar_descripcion("Hay luz, pero no se puede ver la fuente. Viejo papel tapiz de rayas verdes y amarillas. No hay muebles.")

recibidor = rpg.Cuarto("Recibidor")
recibidor.fijar_descripcion("Maceteras florecidas a lo largo de las paredes.")

estudio = rpg.Cuarto("Estudio")
estudio.fijar_descripcion("Una mesa de dibujo, un escritorio y una mesa de trabajo. Cada cual con su propia lámpara. Hay una pizarra con papeles fijados con chinches.")

# Cuartos adyacentes
# El diccionario que almacena los vinculos es cuartos_adyacentes (Plural)
# El metodo para fijar los vinculos es cuarto_adyacente (Singular)

sotano.cuarto_adyacente(cocina, "Sur")

cocina.cuarto_adyacente(sotano, "Norte")
cocina.cuarto_adyacente(comedor, "Este")
cocina.cuarto_adyacente(salon, "Oeste")

comedor.cuarto_adyacente(cocina, "Oeste")
comedor.cuarto_adyacente(recibidor, "Sur")

invernadero.cuarto_adyacente(salon, "Este")
invernadero.cuarto_adyacente(estudio, "Sur")

salon.cuarto_adyacente(cocina, "Este")
salon.cuarto_adyacente(estar, "Sur")
salon.cuarto_adyacente(invernadero, "Oeste")

billar.cuarto_adyacente(biblioteca, "Norte")

recibidor.cuarto_adyacente(comedor, "Norte")
recibidor.cuarto_adyacente(estar, "Oeste")
recibidor.cuarto_adyacente(biblioteca, "Sur")

estudio.cuarto_adyacente(invernadero, "Norte")
estudio.cuarto_adyacente(biblioteca, "Este")

estar.cuarto_adyacente(salon, "Norte")
estar.cuarto_adyacente(recibidor, "Este")

biblioteca.cuarto_adyacente(recibidor,"Norte")
biblioteca.cuarto_adyacente(estudio,"Oeste")
biblioteca.cuarto_adyacente(billar,"Sur")



# Personajes Enemigos
 
gargola = rpg.Enemigo("Gary", "Una gárgola esculpida en piedra.")
gargola.fijar_dialogo("Yo soy el ser más bello en esta mansión.")
gargola.fijar_debilidad("Espejo")
salon.fijar_personaje(gargola)

fantasma = rpg.Enemigo("Abigail", "Un fantasma enojado")
fantasma.fijar_dialogo("Si no me dejas en paz voy a poseer tu cuerpo.")
fantasma.fijar_debilidad("Pluma")
comedor.fijar_personaje(fantasma)

vampiro = rpg.Enemigo("Vim", "Un vampiro. Hace calor, pero el lleva una capa.")
vampiro.fijar_dialogo("Eternidad es un negocio solitarios. Quieres ser mi amigo?")
vampiro.fijar_debilidad("Ataud")
estudio.fijar_personaje(vampiro)

# Amigo Characters
cabra = rpg.Amigo("Cabra", "Una cabra, vos sabés... mamífero, peludo, ojos extraños, pesuñas.")
cabra.fijar_dialogo("Sabés que no soy una cabra cualquiera? Yo soy LA cabra")
cabra.fijar_pista("Gary le teme a la verdad.")
billar.fijar_personaje(cabra)

bailarina = rpg.Amigo("bailarina", "Es un gorda bailarina de vientre.")
bailarina.fijar_dialogo("Yo queria ser una pintora, pero mi mamá quería una bailarina.")
bailarina.fijar_pista("Vim es claustrofobico")
biblioteca.fijar_personaje(bailarina)

Dentista = rpg.Amigo("Dentista", "Un dentista. Pero no viste bata blanca, sino que pijamas de operación.")
Dentista.fijar_dialogo("Dos datos divertidos: las pijamas son menos intimidantes y los dentistas tiene una tasa de suicidios elevada. El humor no es lo mío.")
Dentista.fijar_pista("Abigail nunca ha reído")
cocina.fijar_personaje(Dentista)

# Objetos
espejo = rpg.Objeto("Espejo")
espejo.fijar_descripcion("Un gran espejo ovalado sobre una mesa pequeña con patas de león.")
recibidor.fijar_objeto(espejo)

sierra = rpg.Objeto("Sierra")
sierra.fijar_descripcion("Es una sierra de marco vieja, pero la hoja es completamente nueva.")
comedor.fijar_objeto(sierra)

ataud = rpg.Objeto("Ataud")
ataud.fijar_descripcion("Un ataud de madera barata, sino fuera por la forma pensarias que es un baúl feo")
sotano.fijar_objeto(ataud)

hacha = rpg.Objeto("Hacha")
hacha.fijar_descripcion("Mango largo y curvo. Cuidado, es bien filosa")
invernadero.fijar_objeto(hacha)

torpedo = rpg.Objeto("Torpedo")
torpedo.fijar_descripcion("Grande, cilindrico y negro. Un extremo es redondeado y en el otro extremo una propela.")
estar.fijar_objeto(torpedo)

pluma = rpg.Objeto("Pluma")
pluma.fijar_descripcion("Pluma de pavo real. Larga y bella.")
billar.fijar_objeto(pluma)

# Logica del Juego

habitacion_actual = recibidor
rumbos = ["Norte", "Sur", "Este", "Oeste"]
salveque =[]
seguir_jugando = True
cosa = []

# text
bienvenida = "Esta es una aventura de texto, escribe que acción vas a tomar. Escribe ayuda para ver que acciones podes tratar."
movimientos = "Podes moverte hacia " + str(rumbos)
ordenes = "o trata: hablar, luchar, abrazar, tomar, inventario, salir"
despedida = "Es una forma tonta de gastar tu vida. Valiente, pero tonta sin embargo."

print(bienvenida)

while seguir_jugando:
    print("\n")
    habitacion_actual.obtener_detalles()
    
    habitante = habitacion_actual.obtener_personaje()
    if habitante is not None:
        habitante.describir()

    cosa = habitacion_actual.obtener_objeto()
    if cosa is not None:
        cosa.describir()
        
    instruccion = input("> ")

    if instruccion.capitalize() in rumbos:
        # Moverse en la dirección indicada
        habitacion_actual = habitacion_actual.mover(instruccion.capitalize())
    
    elif instruccion.lower() == "hablar":
        # Hablar con el habitante, si hay uno
        if habitante is not None:
            habitante.hablar()
        else:
            print("Estas hablando solo?")

    elif instruccion.lower() == "luchar":
        # Luchar contra el habitante, si hay alguno
        if habitante is None:
            print("No hay con quien luchar aquí")
        else:  
            print("Que vas a usar como arma?")
            luchar_con = input().capitalize()

            # Tengo ese objeto?
            if luchar_con in salveque:
                
                if habitante.luchar(luchar_con):
                    habitacion_actual.personaje = None
                    print("Hurra, ganaste la batalla!")
                    if gargola.oponentes == 0:
                        print("Felicidades, has vencido las hordas del mal!")
                        seguir_jugando = False
                else:
                    seguir_jugando = False
            else:
                print("No tienes " + pelear_con + " en tu salveque")
       
    elif instruccion.lower() == "tomar":
        if cosa is not None:
            print("Vos has puesto " + cosa.obtener_nombre() + " en tu salveque")
            salveque.append(cosa.obtener_nombre())
            habitacion_actual.fijar_objeto(None)
        else:
            print("No hay nada que tomar aquí")

    elif instruccion.lower() == "abrazar":
        # Abrazar al habitante, si hay alguno
        if habitante is not None:
            habitante.abrazar()
        else:
            print("No hay a quien abrazar aquí")
          
    elif instruccion.lower() == "ayuda":
        print(movimientos)
        print(ordenes)

    elif instruccion.lower() == "inventario":
        if salveque == []:
            print("No hay nada en tu salveque")
        else:
            print("En tu salveque hay los siguientes objetos:")
            print(salveque)
            print("Enemigos = " + str(gargola.oponentes)) # Porque Enemigo.oponente no funciona ?

    elif instruccion.lower() == "salir":
        seguir_jugando = False
        good_bye = "Cobarde, huye mientras puedas!"

    else:
        print("Yo no se como " + instruccion)

# Fin del Juego
fin = "FIN DEL JUEGO"
linea = "-" * len(fin)
print(linea)
print(fin)
print(linea)

if gargola.oponentes != 0:
  print(good_bye)
else:
  print("Felicitaciones. Vos tuviste éxito en esta contienda!")
