# https://gitlab.com/yn1v/oopython_course
# Neville A. Cross
#

from .personaje import Personaje, Enemigo, Amigo
from .objeto import Objeto
from .cuarto import Cuarto
