"""
https://gitlab.com/yn1v/oopython_course 
Neville A. Cross
"""

class Personaje():
    # Crear un personaje
    def __init__(self, nombre_per, descripcion_per):
        """Instanciar un personaje"""
        self.nombre = nombre_per
        self.descripcion = descripcion_per
        self.dialogo = None

  # Describe este personae
    def describir(self):
        """Imprimir la descripción del personaje"""
        print( self.nombre + " esta aquí!" )
        print( self.descripcion )

  # Fijar que dirá este personaje en su diálogo
    def fijar_dialogo(self, dialogo):
        """Fijar que dirá el personaje"""
        self.dialogo = dialogo

  # Hablar con este personaje
    def hablar(self):
        """Hace que el personaje diga su diálogo"""
        if self.dialogo is not None:
            print("[" + self.nombre + " dice]: " + self.dialogo)
        else:
            print(self.nombre + " no quiere hablarte")

  # Luchar contra este personaje
    def luchar(self, arma_objeto):
        """Rutina de lucha pre-definida"""
        print(self.nombre + " no desea luchar contra tí")
        return True

    def abrazar(self):
        """Rutina de abrazo pre-definida"""        
        print("[" + self.nombre + " dice]: Tu no quieres hacer eso")

        
class Enemigo(Personaje):

    oponentes = 0
    
    def __init__(self, nombre_per, descripcion_per):
        """Crea un personaje de la sub-clase Enemigo"""
        super().__init__(nombre_per, descripcion_per)
        self.debilidad = None
        Enemigo.oponentes += 1 # Cuando se crea un enemigo incrementa

    def fijar_debilidad(self, objeto_debilidad):
        """Definir la debilidad del enemigo"""
        self.debilidad = objeto_debilidad
        
    def obtener_debilidad(self):
        """Devolver la debilidad del enemigo"""
        return self.debilidad
  
    # Lucha contra un enemigo
    def fight(self, luchar_con):
        """Rutina de lucha contra un enemigo"""
        if luchar_con == self.debilidad:
            print("Vos derrotaste a " + self.nombre + " con " + combat_item)
            Enemigo.oponentes -= 1
            return True
        else:
            print(self.nombre + " te aplastó, desafortunado aventurero!")
            return False

    
class Amigo(Personaje):
    def __inti__(self, nombre_per, descripcion_per):
        """Crea un personaje de la sub-clase amigo"""
        super().__init__(nombre_per, descripcion)
        self.pista = "Buena suerte"
        
    def fijar_pista(self, pista):
        """Define la pista a brindar"""        
        self.pista = pista
    
    def obtener_pista(self):
        """Devuelve la pista del amigo"""
        return self.pista
  
    def abrazar(self):
        """Rutina de abrazo para un amigo"""
        print(self.nombre + " te abraza a ti!")
        print("[" + self.nombre + " dice]: " + self.obtener_pista())
