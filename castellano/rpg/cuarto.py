"""
https://gitlab.com/yn1v/oopython_course
Neville A. Cross
"""

class Cuarto():
    def __init__(self, nombre_cuarto):
        """Crea un cuarto"""
        self.nombre = nombre_cuarto
        self.descripcion = None
        self.cuartos_adyacentes = {}
        self.objeto = None
        self.personaje = None

    def fijar_personaje(self, nuevo_personaje):
        """Fija que personaje estara ubicado en la habitación """
        self.personaje = nuevo_personaje
    
    def obtener_personaje(self):
        """Devuelve el personaje ubicado en la habitación """
        return self.personaje

    def fijar_descripcion(self, descripcion_cuarto):
        """Fija la descripción para una habitación"""
        self.descripcion = descripcion_cuarto

    def obtener_descripcion(self):
        """Devuelve la descripción de la habitación """
        return self.descripcion

    def obtener_nombre(self):
        """Devolver el nombre de la habitación"""
        return self.nombre

    def fijar_nombre(self, nombre_cuarto):
        """Fijar el nombre de la habitación """
        self.nombre = nombre_cuarto

    def fijar_objeto(self, nombre_objeto):
        """Fijar el objeto ubicado en la habitación """
        self.objeto = nombre_objeto

    def obtener_objeto(self):
        """Devuelve el objeto ubicado en la habitación"""
        return self.objeto

    def describir(self):
        """Devuelve la descripción de la habitación """
        print(self.descripcion)

    def cuarto_adyacente(self, cuarto_a_ubicar, rumbos):
        """Establece una habitación adyacente en el rumbo brindado"""
        self.cuartos_adyacentes[rumbos] = cuarto_a_ubicar
        
    def obtener_detalles(self):
        """Obtener todos los detalles de la habitación"""
        print(self.nombre)
        linea = "-" * len(self.nombre)
        print(linea)
        print(self.descripcion)
        for rumbos in self.cuartos_adyacentes:
            cuarto = self.cuartos_adyacentes[rumbos]
            print(cuarto.obtener_nombre(), "está al", rumbos)
    
    def mover(self, rumbos):
        """rutina para moverse en una dirección """
        if rumbos in self.cuartos_adyacentes:
            return self.cuartos_adyacentes[rumbos]
        else:
            print("No puedes ir en esa dirección")
            return self

  
