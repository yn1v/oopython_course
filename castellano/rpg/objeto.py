"""
https://gitlab.com/yn1v/oopython_course
Neville A. Cross
"""

class Objeto():
    def __init__(self, nombre_objeto):
        """Crear un Objeto"""
        self.nombre = nombre_objeto
        self.descripcion = None

    def fijar_nombre(self, nombre_objeto):
        """ Fijar el nombre del Objeto"""
        self.name = nombre_objeto

    def obtener_nombre(self):
        """ Devolver el nombre del objeto """
        return self.nombre

    def fijar_descripcion(self, descripcion_objeto):
        """ Fijar la descripción del objeto """
        self.descripcion = descripcion_objeto

    def obtener_descripcion(self):
        """ Devuelve la descripción del objeto """
        return self.descripcion

    def describir(self):
        """ Devuelve la descripción del objeto """
        print("[" + self.nombre + "] está en esta habitación - " + self.descripcion)
