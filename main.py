# https://gitlab.com/yn1v/oopython_course
# Neville A. Cross
# GPLv3
# March, 2019
#

import rpg

# Rooms 
kitchen = rpg.Room("Kitchen")
kitchen.set_description("The only light is the fire, which produces smoke. The floor is wet and sliperry")

ballroom = rpg.Room("Ballroom")
ballroom.set_description("Crystal chandelliers hang from the tall ceiling. Black and white checkered floor")

conservatory = rpg.Room("Conservatory")
conservatory.set_description("Glass roof and walls. A fountain and two rock benches. Two workbenches full of plants along the walls. Humid air with the smell of mint")

dining = rpg.Room("Dinning Room")
dining.set_description("Big table for twelve people. Illuminated from candles on the table. The grave people on the canvas appears to do a sad dance.")

cellar = rpg.Room("Cellar")
cellar.set_description("Flickering light and uneven floor. Many shelves.")

billard = rpg.Room("Billard Room")
billard.set_description("There is a low lamp over the pool table in center. Chank finger marks everywhere.")

library = rpg.Room("Library")
library.set_description("Confortable chairs and a velvet divan. Good light and thounsands of books in several languages.")

lounge = rpg.Room("Lounge")
lounge.set_description("There is light, but you can not see the source. Old wallpaper with yellow and green stipes. No furniture.")

hall = rpg.Room("Hall")
hall.set_description("Flower pots blossoming along the walls.")

study = rpg.Room("Study")
study.set_description("A drawing table, a writing table and a workbench. Each with their own lamp. There is a board with papers pinned on it.")

# Linked rooms
cellar.link_room(kitchen, "South")

kitchen.link_room(cellar, "North")
kitchen.link_room(dining, "East")
kitchen.link_room(ballroom, "West")

dining.link_room(kitchen, "West")
dining.link_room(hall, "South")

conservatory.link_room(ballroom, "East")
conservatory.link_room(study, "South")

ballroom.link_room(kitchen, "East")
ballroom.link_room(lounge, "South")
ballroom.link_room(conservatory, "West")

billard.link_room(library, "North")

hall.link_room(dining, "North")
hall.link_room(lounge, "West")
hall.link_room(library, "South")

study.link_room(conservatory, "North")
study.link_room(library, "East")

lounge.link_room(ballroom, "North")
lounge.link_room(hall, "East")

library.link_room(hall,"North")
library.link_room(study,"West")
library.link_room(billard,"South")



# Enemy Characters
 
gargoyle = rpg.Enemy("Gary", "An ugly Gargoyle carved out of stone.")
gargoyle.set_conversation("I am the most beautiful being in this house.")
gargoyle.set_weakness("Mirror")
ballroom.set_character(gargoyle)

ghost = rpg.Enemy("Abigail", "An angry ghost")
ghost.set_conversation("If you don't leave me alone I will posses your body.")
ghost.set_weakness("Feather")
dining.set_character(ghost)

vampire = rpg.Enemy("Vim", "A vampire. It is hot, but he is wearing a cape.")
vampire.set_conversation("Eternety is a lonely business. Want to be my buddy?")
vampire.set_weakness("Coffin")
study.set_character(vampire)

# Friend Characters
goat = rpg.Friend("Goat", "A goat, you know... mammal, furry, weird eyes, hoves")
goat.set_conversation("You know, I am not any goat, I am THE goat")
goat.set_hint("Gary is afraid of thuth.")
billard.set_character(goat)

dancer = rpg.Friend("Dancer", "It is a fat belly dancer.")
dancer.set_conversation("I wanted to be a painter, but mom wanted to have a ballerina")
dancer.set_hint("Vim is claustrophobic")
library.set_character(dancer)

dentist = rpg.Friend("Dentist", "It is a dentist, but he doesn't wear a white coat, only scrubs")
dentist.set_conversation("Two fun facts: scrubs are least threatening and dentist have high rate of suicides. Humor is not my thing.")
dentist.set_hint("Abigail haven't laught ever")
kitchen.set_character(dentist)

# Items
mirror = rpg.Item("Mirror")
mirror.set_description("Big mirror over a small table with lion like legs.")
hall.set_item(mirror)

saw = rpg.Item("Saw")
saw.set_description("It is a old Hack saw, but the blade is brand new.")
dining.set_item(saw)

coffin = rpg.Item("Coffin")
coffin.set_description("Cheap wooden coffin, if not for the shape you will think is a bad trunk")
cellar.set_item(coffin)

axe = rpg.Item("Axe")
axe.set_description("Long curved handle. Careful, it is very sharp")
conservatory.set_item(axe)

torpedo = rpg.Item("Torpedo")
torpedo.set_description("Big, cilindrical, black. One end is round and there is a propeller on the other end.")
lounge.set_item(torpedo)

feather = rpg.Item("Feather")
feather.set_description("Peacock feather. Long and beautiful")
billard.set_item(feather)

# Game logic

current_room = hall
directions = ["North", "South", "East", "West"]
backpack =[]
keep_playing = True
things = []

# text
welcome = "This is a text adventure, type in what you want to do. Type help to see the actions that you can try."
movements = "You can move " + str(directions)
orders = "or try: talk, fight, hug, take, inventory"
good_bye = "It was a dumb way to waste your life. Valient, but dumb nevertheless."

print(welcome)

while keep_playing:
    print("\n")
    current_room.get_details()
    
    inhabitant = current_room.get_character()
    if inhabitant is not None:
        inhabitant.describe()

    thing = current_room.get_item()
    if thing is not None:
        thing.describe()
        
    command = input("> ")

    if command.capitalize() in directions:
        # Move in the given direction
        current_room = current_room.move(command.capitalize())
    
    elif command.lower() == "talk":
        # Talk to the inhabitant, if there is one
        if inhabitant is not None:
            inhabitant.talk()
        else:
            print("Are you talking alone?")

    elif command.lower() == "fight":
        # Fight with the inhabitant if there is one    
        if inhabitant is None:
            print("There is no one here to fight with")
        else:  
            print("What will you fight with?")
            fight_with = input().capitalize()

            # Do I have this item ?
            if fight_with in backpack:
                
                if inhabitant.fight(fight_with):
                    current_room.character = None
                    print("Hooray, you won the fight!")
                    if gargoyle.foes == 0:
                        print("Congratulations, you have vanquished the enemy horde!")
                        keep_playing = False
                else:
                    keep_playing = False
            else:
                print("you don't have " + fight_with + " in your backpak")
       
    elif command.lower() == "take":
        if thing is not None:
            print("You put the " + thing.get_name() + " in your backpack")
            backpack.append(thing.get_name())
            current_room.set_item(None)
        else:
            print("There is nothing here to take")

    elif command.lower() == "hug":
        # Hug the inhabitant, if there is one
        if inhabitant is not None:
            inhabitant.hug()
        else:
            print("There is nobody to hug here")
          
    elif command.lower() == "help":
        print(movements)
        print(orders)

    elif command.lower() == "inventory":
        if backpack == []:
            print("There is nothing in your back pack")
        else:
            print("In your backpack there are the following items:")
            print(backpack)
            print("Enemies = " + str(gargoyle.foes)) # Why Enemy.foes do not work ?

    elif command.lower() == "quit":
        keep_playing = False
        good_bye = "Coward, run while you can!"

    else:
        print("I don't know how to " + command)

# Game over
go = "GAME OVER"
line = "-" * len(go)
print(go)
print(line)

if gargoyle.foes != 0:
  print(good_bye)
else:
  print("Congratulations. You succeded in this quest!")
